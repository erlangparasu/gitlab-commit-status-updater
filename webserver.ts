/**
 * Created by: Erlang Parasu (erlangparasu) 2022.
 */
import { serve } from "https://deno.land/std@0.135.0/http/server.ts";
import "https://deno.land/x/dotenv@v3.2.0/load.ts";

const port = 8383;
const token = Deno.env.get("X_APP_ACCESS_TOKEN") ?? "";

const handler = (request: Request): Response => {
  let body = `Your user-agent is:\n\n${
    request.headers.get("user-agent") ?? "Unknown"
  }`;

  const url = request.url;
  const arr = url.split("/");

  const dataList = Array<string>();
  let startPush = false;

  arr.forEach((segment, i) => {
    if ("commit-status" == segment) {
      startPush = true;
    } else {
      if (startPush) {
        dataList.push(segment);
      }
    }
  });

  try {
    const projectId: string = dataList[0];
    const commitHash: string = dataList[1];
    const stageName: string = dataList[2];
    const stageStatus: string = dataList[3];
    if (["build", "test", "deploy"].includes(stageName)) {
      if (
        ["pending", "running", "success", "failed", "canceled"].includes(
          stageStatus,
        )
      ) {
        body = dataList.join(", ");
        let data = {
          projectId,
          commitHash,
          stageName,
          stageStatus,
        };

        ajaxUpdateStatus(data);
      }
    }
  } catch (error) {
    //
  }

  return new Response(body, { status: 200 });
};
async function ajaxUpdateStatus(
  data: {
    projectId: string;
    commitHash: string;
    stageName: string;
    stageStatus: string;
  },
) {
  // data.stageName = (new Date()).getTime().toString() + "-" + data.stageName;

  let url = "https://gitlab.com/api/v4/projects/" + data.projectId +
    "/statuses/" + data.commitHash +
    "?state=" + data.stageStatus +
    "&name=" + data.stageName +
    // "&target_url=" + data.stageName +
    // "&description=" + data.stageName +
    "";

  let response = await postData(url, {});
  console.log(response);

  return response;
}

// Example POST method implementation:
async function postData(url = "", data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    // mode: "cors", // no-cors, *cors, same-origin
    cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    // credentials: "same-origin", // include, *same-origin, omit
    headers: {
      // "Content-Type": "application/json",
      "Content-Type": "application/x-www-form-urlencoded",
      "PRIVATE-TOKEN": token,
    },
    // redirect: "follow", // manual, *follow, error
    referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    // body: JSON.stringify(data), // body data type must match "Content-Type" header
  });

  return response.json(); // parses JSON response into native JavaScript objects
}

console.log(`HTTP webserver running. Access it at: http://localhost:${port}/`);
await serve(handler, { port });
