# GitLab Commit Status Updater

An API server to update commit status on GitLab

## How to use:
```bash
cp .env.example .env

./server.sh
```

API Endpoints:

- To update commit status:
    ```
    GET http://localhost:<port>/update/commit-status/<project_id>/<commit_hash>/<state_name>/<state_status>
    ```

    Example:
    ```
    GET http://localhost:8383/update/commit-status/123/ABCDEFGH/build/running
    ```


## Requirement:
- deno 1.20.6


## Author:
- ErlangParasu 2022
